%if 0%{?fedora}
%global with_python3 1
%endif

%define srcname beanbag

Name:           python-beanbag
Version:        1.9.2
Release:        1%{?dist}
Summary:        A helper module for accessing REST APIs
License:        MIT
URL:            https://github.com/ajtowns/beanbag
Source0:        http://pypi.python.org/packages/source/b/%{srcname}/%{srcname}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  pytest
BuildRequires:  python2-devel
BuildRequires:  python-requests
%if 0%{?with_python3}
BuildRequires:  python3-devel
BuildRequires:  python3-requests
BuildRequires:  python3-pytest
%endif # with_python3

%description
BeanBag is a simple module that lets you access REST APIs in an easy way.

%package -n python2-%{srcname}
Summary:        %{summary}
Requires:       python-requests
%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname}
BeanBag is a simple module that lets you access REST APIs in an easy way.

%if 0%{?with_python3}
%package -n python3-beanbag
Summary:        %{summary}
Requires:       python3
Requires:       python3-requests

%description -n python3-beanbag
BeanBag is a simple module that lets you access REST APIs in an easy way.
%endif # with_python3

%prep
%autosetup -n %{srcname}-%{version}

%build
%{py2_build}

%if 0%{?with_python3}
%{py3_build}
%endif # with_python3

%install
%py2_install

%if 0%{?with_python3}
%py3_install
%endif # with_python3

%check
export PYTHONPATH=$(pwd)

py.test-%{python2_version} -v tests

%if 0%{?with_python3}
py.test-%{python3_version} -v tests
%endif # with_python3

%files -n python2-%{srcname}
%doc README.rst
%license LICENSE
%{python2_sitelib}/*

%if 0%{?with_python3}
%files -n python3-%{srcname}
%doc README.rst
%license LICENSE
%{python3_sitelib}/*
%endif # with_python3

%changelog
* Sat Dec 12 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.9.2-1
- Initial package
